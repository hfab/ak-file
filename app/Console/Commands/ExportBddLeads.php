<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ExportBddLeads extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'export:lead';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
          $hier = new \DateTime('-1 day');
          // echo $hier -> format('Y-m-d');
          
          $query = \DB::table('leads')
                ->select('date','campagne','nom','prenom','email','telephone','formation','situationpro','nbanneesalariat')
                ->where('created_at','LIKE', $hier->format('Y-m-d').'%')
                ->get();
                
        $today = date("Ymd");
        $filetoftp = 'export_leads_'.$hier->format('Y-m-d').'.csv';
        $localfile = storage_path().'/api/' . $filetoftp;
        
        $fp = fopen($localfile,"w+");
        fclose($fp);
        
        $content = 'date_inscription;campagne;nom;prenom;email;telephone;formation;situation;duree' ."\n";
        
        $fp = fopen($localfile,"a+");
        fwrite($fp, $content);
        fclose($fp);
        
        $content = ''; 
        foreach($query as $q){
            
            var_dump($q); 
            $content .=  $q->date.';'.$q->campagne.';'.$q->nom.';'.$q->prenom.';'.$q->email.';'.$q->telephone.';'.$q->formation.';'. $q->situationpro.';'.$q->nbanneesalariat."\n";
            
        }
        
        $fp = fopen($localfile,"a+");
        fwrite($fp, $content);
        fclose($fp);
            
        $this->call('livraison:curl');
    }
}
