<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class LivraisonFTPcurl extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
     //dynamic
    // protected $signature = 'livraison:curl {file}';
    protected $signature = 'livraison:curl';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Envoi fichier ftp via curl';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        function upload($local, $remote) {
        
        $server = '13.94.253.6';
        $username = 'Citrus';
        $password = 'Dv&/UL6h$zh7thRz'; 
              
        if ($fp = fopen($local, 'r')) {
            $ftp_server = 'ftp://' . $server . '/' . $remote;
            $ch = curl_init();

            curl_setopt($ch, CURLOPT_URL, $ftp_server);
            curl_setopt($ch, CURLOPT_USERPWD, $username . ':' . $password);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
            curl_setopt($ch, CURLOPT_FTP_SSL, CURLFTPSSL_TRY);
            curl_setopt($ch, CURLOPT_FTPSSLAUTH, CURLFTPAUTH_TLS);
            curl_setopt($ch, CURLOPT_UPLOAD, 1);
            curl_setopt($ch, CURLOPT_INFILE, $fp);

            curl_exec($ch);
            $err = curl_error($ch);
            curl_close($ch);

            return !$err;
        }
        return false;
    }
    
    // dynamic
    // $filetoftp = $this->argument('file');
    
    $hier = new \DateTime('-1 day');
    //$today = date("Ymd");
    $datehier = $hier->format('Y-m-d');
    
    $filetoftp = 'export_leads_'.$datehier.'.csv';
    $localfile = storage_path().'/api/' . $filetoftp;
    upload( $localfile,'livraison_citrusp_'.$datehier.'.csv');
    }
}
