<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ApiFile extends Controller
{

    public function get_json(Request $request){
    $data = $request->json()->all();

    $duplicate = \DB::table('leads')
    ->where('campagne', $data["campagne"])
    ->where('email', $data["email"])
    ->where('telephone', $data["telephone"])
    ->count();
    
    if($duplicate > 0){
        
        return response()->json([
                "success" => false,
                "message" => "DOUBLON > Le lead existe déja"
            ]);
    }

    $today = date("Y-m-d H:i:s");
    $dataset[] = [
    'date' => $data["date"],
    'campagne' => $data["campagne"],
    'nom' => $data["nom"],
    'prenom' => $data["prenom"],
    'email' => $data["email"],
    'telephone' => $data["telephone"],
    'formation' => $data["formation"],
    'situationpro' => $data["situationpro"],
    'nbanneesalariat' => $data["nbanneesalariat"],
    'ip' => $data["ip"],
    'source' => $data["source"],
    'created_at' => $today,
    'updated_at' => $today
    ];

    \DB::table('leads')->insert($dataset);
            return response()->json([
                "success" => true,
                "message" => "OK > Le lead a bien été enregistré"
            ]);
}


public function upload(Request $request) 
    { 
        
        /*
        $validator = Validator::make($request->all(),[ 
              'file' => 'required|mimes:doc,docx,pdf,txt,csv|max:2048',
        ]);   
  
        if($validator->fails()) {          
             
            return response()->json(['error'=>$validator->errors()], 401);                        
         }  
  
   
        if ($file = $request->file('file')) {
            $path = $file->store('public/files');
            $name = $file->getClientOriginalName();
  
            //store your file into directory and db
            $save = new File();
            $save->name = $file;
            $save->store_path= $path;
            $save->save();
            
            */
               
            return response()->json([
                "success" => true,
                "message" => "File successfully uploaded"
                //"file" => $file
            ]);
   
        }

}
