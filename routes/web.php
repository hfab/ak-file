<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiFile;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::get('uploading-file-api', [ApiFile::class, 'upload']);

Route::post('api-comc', [ApiFile::class, 'get_json']);
Route::post('upload-api', [ApiFile::class, 'upload']);