<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Leads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
    Schema::create('leads', function (Blueprint $table) {
        $table->id();
        $table->string('date');
        $table->string('campagne');
        $table->string('nom');
        $table->string('prenom');
        $table->string('email');
        $table->string('telephone');
        $table->string('formation');
        $table->string('situationpro');
        $table->string('nbanneesalariat');
        $table->string('ip');
        $table->string('source');
        $table->timestamps();
    });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('leads');
    }
}
